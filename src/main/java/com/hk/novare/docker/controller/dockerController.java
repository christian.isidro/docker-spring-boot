package com.hk.novare.docker.controller;


import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class dockerController {

    @GetMapping(value="/hello/{name}")
    public String getAllBooks(@PathVariable String name) {
        return "Hello!" + name;
    }


}
