FROM openjdk:11

USER root

ADD "./target/docker-spring-boot.jar" "./app.jar"

EXPOSE 8085

CMD ["java", "-jar", "-Djava.security.egd=file:/dev/./urandom", "./app.jar"]